#ifndef HELPER_H
#define HELPER_H

#include "types.h"
#include <string>
#include <vector>
#include <sys/time.h>
#include "clipped_vd.h"
#include "poisson.h"

class Timer
{
public:
    Timer() {}

    void start()
    {
        gettimeofday(&_start, NULL);
        _records.push_back(0);
    }

    double stop()
    {
        gettimeofday(&_end, NULL);
        double delta = ((_end.tv_sec  - _start.tv_sec) * 1000000u +
                        _end.tv_usec - _start.tv_usec) / 1.e6;
        _records.push_back(delta);
        return delta - _records[_records.size()-2];
    }

public:
    std::vector<double> _records;
    timeval _start;
    timeval _end;
};

double random_double(double lower, double upper);
Point random_point(const Bbox &bbox);
Vector random_vector();

void split(std::string s, std::string delimiter, std::vector<std::string> &tokens);
void polyhedron2triangles(Polyhedron &polyhedron, std::vector<Triangle> &triangles);

void save(std::string file, std::vector<Point> &points);

Point center(Triangle t);
Triangle orient(Point p, Triangle t);
Triangle_exact exact(Triangle t);
Triangle inexact(Triangle_exact t);
void intersection(Triangle_exact t1, Triangle_exact t2, std::vector<Point_exact> &constraints);
void cdt_2d(Triangle_exact t, std::vector<Point_exact> &constraints, std::vector<Triangle_exact> &out);

void save(std::string file, Clipped_VD &cvd, double shrink = 0.8);

Point bisection(Poisson *poisson, Point p0, Point p1);

Point center(std::vector<Point> &points);
Point center(std::set<Point> &points);
void lloyd(Surface *surface, std::vector<Point> &sites, int nb_iters = 5);

#endif // HELPER_H
