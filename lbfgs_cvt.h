#ifndef LBFGS_CVT_H
#define LBFGS_CVT_H

#include "surface.h"
#include <lbfgs.h>

class LBFGS_CVT
{
public:
    LBFGS_CVT(Surface *surface, std::vector<Point> &sites): _surface(surface), _sites(sites), m_x(NULL) {}
    virtual ~LBFGS_CVT()
    {
        if (m_x != NULL) {
            lbfgs_free(m_x);
            m_x = NULL;
        }
    }
    int run();

protected:
    static lbfgsfloatval_t _evaluate(
            void *instance,
            const lbfgsfloatval_t *x,
            lbfgsfloatval_t *g,
            const int n,
            const lbfgsfloatval_t step
            )
    {
        return reinterpret_cast<LBFGS_CVT*>(instance)->evaluate(x, g, n, step);
    }

    lbfgsfloatval_t evaluate(const lbfgsfloatval_t *x,
                             lbfgsfloatval_t *g,
                             const int n,
                             const lbfgsfloatval_t step
                             );

    static int _progress(
            void *instance,
            const lbfgsfloatval_t *x,
            const lbfgsfloatval_t *g,
            const lbfgsfloatval_t fx,
            const lbfgsfloatval_t xnorm,
            const lbfgsfloatval_t gnorm,
            const lbfgsfloatval_t step,
            int n,
            int k,
            int ls
            )
    {
        return reinterpret_cast<LBFGS_CVT*>(instance)->progress(x, g, fx, xnorm, gnorm, step, n, k, ls);
    }

    int progress(
            const lbfgsfloatval_t *x,
            const lbfgsfloatval_t *g,
            const lbfgsfloatval_t fx,
            const lbfgsfloatval_t xnorm,
            const lbfgsfloatval_t gnorm,
            const lbfgsfloatval_t step,
            int n,
            int k,
            int ls
            );

public:
    Surface *_surface;
    std::vector<Point> _sites;

protected:
    lbfgsfloatval_t *m_x;
};

#endif // LBFGS_CVT_H
