#ifndef MESH_H
#define MESH_H

#include "surface.h"
#include <string>

class Mesh: public Surface
{
public:
    Mesh(std::string file);

    void load(std::string file);
    bool is_interior(Point query);
    void interior_sample(int nb, std::vector<Point> &points);

public:
    std::vector<Triangle> _triangles;
    Tree *_tree;
};

#endif // MESH_H
