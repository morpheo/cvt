#ifndef TYPES_H
#define TYPES_H

// IO
#include <CGAL/IO/Polyhedron_iostream.h>
#include <CGAL/IO/read_xyz_points.h>

// Kernel
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>

// Primitives 3D
#include <CGAL/Point_3.h>
#include <CGAL/Segment_3.h>
#include <CGAL/Vector_3.h>
#include <CGAL/Ray_3.h>
#include <CGAL/Triangle_3.h>
#include <CGAL/Plane_3.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Sphere_3.h>
#include <CGAL/Bbox_3.h>

// Primitives 2D
#include <CGAL/Point_2.h>

// AABB tree
#include <CGAL/AABB_triangle_primitive.h>
#include <CGAL/AABB_traits.h>
#include <CGAL/AABB_tree.h>

// Poisson
#include <CGAL/Point_with_normal_3.h>
#include <CGAL/Poisson_reconstruction_function.h>

// 3D Delaunay
#include <CGAL/Triangulation_vertex_base_3.h>
#include <CGAL/Triangulation_vertex_base_with_info_3.h>
#include <CGAL/Triangulation_cell_base_3.h>
#include <CGAL/Triangulation_data_structure_3.h>
#include <CGAL/Delaunay_triangulation_3.h>

// 2D constrained Delaunay
#include <CGAL/Triangulation_vertex_base_2.h>
#include <CGAL/Triangulation_face_base_2.h>
#include <CGAL/Constrained_triangulation_face_base_2.h>
#include <CGAL/Triangulation_data_structure_2.h>
#include <CGAL/Constrained_Delaunay_triangulation_2.h>
#include <CGAL/Constrained_triangulation_2.h>

// Neighbor search
#include <CGAL/basic.h>
#include <CGAL/Search_traits_3.h>
#include <CGAL/Search_traits_adapter.h>
#include <CGAL/Orthogonal_k_neighbor_search.h>
#include <CGAL/property_map.h>
#include <boost/iterator/zip_iterator.hpp>
#include <utility>

typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
typedef CGAL::Exact_predicates_exact_constructions_kernel Kernel_exact;

typedef CGAL::Point_3<Kernel> Point;
typedef CGAL::Segment_3<Kernel> Segment;
typedef CGAL::Vector_3<Kernel> Vector;
typedef CGAL::Ray_3<Kernel> Ray;
typedef CGAL::Triangle_3<Kernel> Triangle;
typedef CGAL::Plane_3<Kernel> Plane;
typedef CGAL::Polyhedron_3<Kernel> Polyhedron;
typedef CGAL::Sphere_3<Kernel> Sphere;

typedef CGAL::Point_3<Kernel_exact> Point_exact;
typedef CGAL::Segment_3<Kernel_exact> Segment_exact;
typedef CGAL::Vector_3<Kernel_exact> Vector_exact;
typedef CGAL::Ray_3<Kernel_exact> Ray_exact;
typedef CGAL::Triangle_3<Kernel_exact> Triangle_exact;
typedef CGAL::Plane_3<Kernel_exact> Plane_exact;
typedef CGAL::Polyhedron_3<Kernel_exact> Polyhedron_exact;
typedef CGAL::Sphere_3<Kernel_exact> Sphere_exact;

typedef CGAL::Point_2<Kernel_exact> Point_2_exact;

typedef CGAL::Bbox_3 Bbox;

typedef CGAL::AABB_triangle_primitive<Kernel, std::vector<Triangle>::iterator> Primitive;
typedef CGAL::AABB_traits<Kernel, Primitive> Traits;
typedef CGAL::AABB_tree<Traits> Tree;

typedef CGAL::Point_with_normal_3<Kernel> Point_with_normal;
typedef CGAL::Poisson_reconstruction_function<Kernel> Poisson_reconstruction_function;

typedef CGAL::Triangulation_vertex_base_3<Kernel> Vertex_base;
typedef CGAL::Triangulation_vertex_base_with_info_3<unsigned int, Kernel, Vertex_base> Vertex_base_with_info;
typedef CGAL::Triangulation_cell_base_3<Kernel> Cell_base;
typedef CGAL::Triangulation_data_structure_3<Vertex_base_with_info, Cell_base> Triangulation_data_structure;
typedef CGAL::Delaunay_triangulation_3<Kernel, Triangulation_data_structure> Delaunay_triangulation;

typedef CGAL::Triangulation_vertex_base_2<Kernel_exact> Vb_exact;
typedef CGAL::Constrained_triangulation_face_base_2<Kernel_exact> Fb_exact;
typedef CGAL::Triangulation_data_structure_2<Vb_exact, Fb_exact> TDS_exact;
typedef CGAL::Exact_predicates_tag Itag;
typedef CGAL::Constrained_Delaunay_triangulation_2<Kernel_exact, TDS_exact, Itag> CDT_exact;

typedef boost::tuple<Point, int> Point_and_int;
typedef CGAL::Search_traits_3<Kernel> Search_traits_base;
typedef CGAL::Search_traits_adapter<Point_and_int, CGAL::Nth_of_tuple_property_map<0, Point_and_int>, Search_traits_base> Search_traits;
typedef CGAL::Orthogonal_k_neighbor_search<Search_traits> Neighbor_search;

extern double EPSILON;
extern double BISEC_EPS;
extern int NB_ITER;
extern int NB_SITES;
extern double SHRINK;

#endif // TYPES_H
