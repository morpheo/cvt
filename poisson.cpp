#include "poisson.h"
#include <fstream>
#include "helper.h"
#include <ctime>
#include <cstdlib>

Poisson::Poisson(std::string file)
{
    std::ifstream in(file.c_str());
    if(!in || !CGAL::read_xyz_points_and_normals(
                in,
                std::back_inserter(_points),
                CGAL::make_normal_of_point_with_normal_pmap(
                    std::vector<Point_with_normal>::value_type())))
        throw std::string("[Error] : cannot load " + file);

    _implicit_function = new Poisson_reconstruction_function(_points.begin(),
                                                             _points.end(),
                                                             CGAL::make_normal_of_point_with_normal_pmap(
                                                                 std::vector<Point_with_normal>::value_type()));

    for(int i = 0; i < _points.size(); ++i)
        if(i == 0)
            _bbox = _points[i].position().bbox();
        else
            _bbox += _points[i].position().bbox();
    std::cout << "[Bbox] : " << _bbox << std::endl;

    if(!_implicit_function->compute_implicit_function())
        std::cout << "[Error] : cannot compute implicit function" << std::endl;
}

bool Poisson::is_interior(Point query)
{
    if(query[0] < _bbox.xmin() || query[0] > _bbox.xmax() ||
            query[1] < _bbox.ymin() || query[1] > _bbox.ymax() ||
            query[2] < _bbox.zmin() || query[2] > _bbox.zmax() )
        return false;

    if((*_implicit_function)(query) >= 0)
        return false;
    return true;
}

void Poisson::interior_sample(int nb, std::vector<Point> &points)
{
    std::srand(std::time(0));
    std::set<Point> pts;
    while(pts.size() < nb)
    {
        Point p = random_point(_bbox);
        if(is_interior(p))
            pts.insert(p);
    }
    points.assign(pts.begin(), pts.end());
}

double Poisson::value(Point query)
{
    if(query[0] < _bbox.xmin() || query[0] > _bbox.xmax() ||
            query[1] < _bbox.ymin() || query[1] > _bbox.ymax() ||
            query[2] < _bbox.zmin() || query[2] > _bbox.zmax() )
        return 1;
    return (*_implicit_function)(query);
}
