#include "types.h"

double EPSILON = 1e-20;
double BISEC_EPS = 1e-4;
int NB_ITER = 30;
int NB_SITES = 3000;
double SHRINK = 0.8;
