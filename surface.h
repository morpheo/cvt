#ifndef SURFACE_H
#define SURFACE_H

#include "types.h"
#include <vector>

class Surface
{
public:
    virtual bool is_interior(Point query) = 0;
    virtual void interior_sample(int nb, std::vector<Point> &points) = 0;
};

#endif // SURFACE_H
