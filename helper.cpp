#include "helper.h"
#include <fstream>
#include "polyhedral_mass_properties.h"

double random_double(double lower, double upper)
{
    return (lower + (upper - lower) * std::rand() / (double)RAND_MAX);
}

Point random_point(const Bbox &bbox)
{
    return Point(random_double(bbox.xmin(), bbox.xmax()),
                 random_double(bbox.ymin(), bbox.ymax()),
                 random_double(bbox.zmin(), bbox.zmax()));
}

Vector random_vector()
{
    double x = random_double(0, 1);
    double y = random_double(0, 1);
    double z = random_double(0, 1);
    double norm = std::sqrt(x*x + y*y + z*z);

    if(norm < EPSILON)
        return random_vector();
    return Vector(x/norm, y/norm, z/norm);
}

void split(std::string s, std::string delimiter, std::vector<std::string> &tokens)
{
    std::string::size_type i = 0;
    std::string::size_type j = s.find(delimiter);

    while (j != std::string::npos) {
        tokens.push_back(s.substr(i, j-i));
        i = ++j;
        j = s.find(delimiter, j);

        if (j == std::string::npos)
            tokens.push_back(s.substr(i, s.length()));
    }
}

void polyhedron2triangles(Polyhedron &polyhedron, std::vector<Triangle> &triangles)
{
    for(Polyhedron::Facet_iterator itr = polyhedron.facets_begin(); itr != polyhedron.facets_end(); ++itr)
    {
        Polyhedron::Halfedge_handle hh = itr->halfedge();
        Triangle t(hh->vertex()->point(),
                   hh->next()->vertex()->point(),
                   hh->opposite()->vertex()->point());
        if(!t.is_degenerate())
            triangles.push_back(t);
    }
}

void save(std::string file, std::vector<Point> &points)
{
    std::ofstream out(file.c_str());
    if(out.is_open())
    {
        for(int i = 0; i < points.size(); ++i)
            out << points[i] << std::endl;
    }
    else
        std::cout << "[Error] : cannot save " << file << std::endl;
}

Point center(Triangle t)
{
    return Point((t[0][0]+t[1][0]+t[2][0])/3,
            (t[0][1]+t[1][1]+t[2][1])/3,
            (t[0][2]+t[1][2]+t[2][2])/3);
}

Triangle orient(Point p, Triangle t)
{
    Vector query(p, t[0]);
    Vector u(t[0], t[1]);
    Vector v(t[0], t[2]);
    Vector n = CGAL::cross_product(u, v);
    if(query * n >= 0)
        return t;
    else
        return Triangle(t[0], t[2], t[1]);
}

Triangle_exact exact(Triangle t)
{
    return Triangle_exact(Point_exact(t[0][0], t[0][1], t[0][2]),
            Point_exact(t[1][0], t[1][1], t[1][2]),
            Point_exact(t[2][0], t[2][1], t[2][2]));
}

Triangle inexact(Triangle_exact t)
{
    return Triangle(Point(CGAL::to_double(t[0][0]), CGAL::to_double(t[0][1]), CGAL::to_double(t[0][2])),
            Point(CGAL::to_double(t[1][0]), CGAL::to_double(t[1][1]), CGAL::to_double(t[1][2])),
            Point(CGAL::to_double(t[2][0]), CGAL::to_double(t[2][1]), CGAL::to_double(t[2][2])));
}

void intersection(Triangle_exact t1, Triangle_exact t2, std::vector<Point_exact> &constraints)
{
    CGAL::Object inter = CGAL::intersection(t1, t2);
    if(CGAL::object_cast<Segment_exact>(&inter))
    {
        Segment_exact s = *CGAL::object_cast<Segment_exact>(&inter);
        constraints.push_back(s[0]);
        constraints.push_back(s[1]);
    }
    if(CGAL::object_cast<Triangle_exact>(&inter))
    {
        Triangle_exact t = *CGAL::object_cast<Triangle_exact>(&inter);
        constraints.push_back(t[0]);
        constraints.push_back(t[1]);
        constraints.push_back(t[1]);
        constraints.push_back(t[2]);
        constraints.push_back(t[2]);
        constraints.push_back(t[0]);
    }
    if(CGAL::object_cast< std::vector<Point_exact> >(&inter))
    {
        std::vector<Point_exact> ps = *CGAL::object_cast< std::vector<Point_exact> >(&inter);
        for(int i=0; i<ps.size(); i++)
        {
            constraints.push_back(ps[i]);
            constraints.push_back(ps[(i+1) % ps.size()]);
        }
    }
}

void cdt_2d(Triangle_exact t, std::vector<Point_exact> &constraints, std::vector<Triangle_exact> &out)
{
    Plane_exact plane = t.supporting_plane();
    CDT_exact cdt;
    cdt.insert(plane.to_2d(t[0]));
    cdt.insert(plane.to_2d(t[1]));
    cdt.insert(plane.to_2d(t[2]));
    for(int i=0; i<constraints.size()/2; ++i)
    {
        Point_2_exact p0 = plane.to_2d(constraints[i*2]);
        Point_2_exact p1 = plane.to_2d(constraints[i*2 + 1]);
        if(p0 != p1)
            cdt.insert_constraint(p0, p1);
    }
    for(CDT_exact::Finite_faces_iterator itr = cdt.finite_faces_begin(); itr != cdt.finite_faces_end(); ++itr)
    {
        CDT_exact::Triangle t_out = cdt.triangle(itr);
        Triangle_exact t_res(plane.to_3d(t_out[0]), plane.to_3d(t_out[1]), plane.to_3d(t_out[2]));
        if(!t_res.is_degenerate())
            out.push_back(t_res);
    }
}

void save(std::string file, Clipped_VD &cvd, double shrink)
{
    std::ofstream out(file.c_str());
    if(out.is_open())
    {
        out << "# nc " << cvd._cells.size() << std::endl;
        int cur = 1;
        for(int i = 0; i < cvd._cells.size(); ++i)
        {
            Voronoi_cell &cell = cvd._cells[i];
            int id = cell._id;
            Point site = cvd._sites[id];

            for(int j = 0; j < cell._bisectors.size(); ++j)
            {
                Bisector &bisector = cell._bisectors[j];
                int s0 = bisector._s0;
                int s1 = bisector._s1;
                for(int k = 0; k < bisector._triangles.size(); ++k)
                {
                    Vector v0(site, bisector._triangles[k][0]);
                    Vector v1(site, bisector._triangles[k][1]);
                    Vector v2(site, bisector._triangles[k][2]);
                    out << "v " << site + shrink * v0 << std::endl;
                    out << "v " << site + shrink * v1 << std::endl;
                    out << "v " << site + shrink * v2 << std::endl;
                    out << "f " << cur << " " << cur + 1 << " " << cur + 2 << std::endl;
                    cur += 3;
                    out << "# f " << ((cur - 1) / 3) - 1 << " " << s0 << " " << s1 << std::endl;
                }
            }

            for(int j = 0; j < cell._rvd.size(); ++j)
            {
                Vector v0(site, cell._rvd[j][0]);
                Vector v1(site, cell._rvd[j][1]);
                Vector v2(site, cell._rvd[j][2]);
                out << "v " << site + shrink * v0 << std::endl;
                out << "v " << site + shrink * v1 << std::endl;
                out << "v " << site + shrink * v2 << std::endl;
                out << "f " << cur << " " << cur + 1 << " " << cur + 2 << std::endl;
                cur += 3;
                out << "# f " << ((cur - 1) / 3) - 1 << " " << id << " " << -1 << std::endl;
            }
        }

        for(int i = 0; i < cvd._vcells.size(); ++i)
        {
            Vcell &cell = cvd._vcells[i];
            Point site = cvd._sites[i];

            for(int j = 0; j < cell._convex_hull.size(); ++j)
            {
                Vector v0(site, cell._convex_hull[j][0]);
                Vector v1(site, cell._convex_hull[j][1]);
                Vector v2(site, cell._convex_hull[j][2]);
                out << "v " << site + shrink * v0 << std::endl;
                out << "v " << site + shrink * v1 << std::endl;
                out << "v " << site + shrink * v2 << std::endl;
                out << "f " << cur << " " << cur + 1 << " " << cur + 2 << std::endl;
                cur += 3;
                out << "# f " << ((cur - 1) / 3) - 1 << " " << i << " " << -2 << std::endl;
            }

            for(int j = 0; j < cell._rvd.size(); ++j)
            {
                Vector v0(site, cell._rvd[j][0]);
                Vector v1(site, cell._rvd[j][1]);
                Vector v2(site, cell._rvd[j][2]);
                out << "v " << site + shrink * v0 << std::endl;
                out << "v " << site + shrink * v1 << std::endl;
                out << "v " << site + shrink * v2 << std::endl;
                out << "f " << cur << " " << cur + 1 << " " << cur + 2 << std::endl;
                cur += 3;
                out << "# f " << ((cur - 1) / 3) - 1 << " " << i << " " << -1 << std::endl;
            }
        }
    }
    else
        std::cerr << "[Error]: cannot save " << file << std::endl;
}

Point bisection(Poisson *poisson, Point p0, Point p1)
{
    double val0 = poisson->value(p0);
    double val1 = poisson->value(p1);

    if(val0 * val1 > 0)
    {
        std::cout << "[Error] : Bisection" << std::endl;
        return p0;
    }

    if(std::abs(val0) < EPSILON)
        return p0;
    if(std::abs(val1) < EPSILON)
        return p1;

    while(CGAL::squared_distance(p0, p1) > BISEC_EPS * BISEC_EPS)
    {
        Point p2 = CGAL::midpoint(p0, p1);
        double val2 = poisson->value(p2);
        if(val2 * val0 > 0)
        {
            val0 = val2;
            p0 = p2;
        }
        else
        {
            val1 = val2;
            p1 = p2;
        }
    }
    return CGAL::midpoint(p0, p1);
}

Point center(std::vector<Point> &points)
{
    double x = 0;
    double y = 0;
    double z = 0;
    for(int i = 0; i < points.size(); ++i)
    {
        x += points[i][0];
        y += points[i][1];
        z += points[i][2];
    }
    return Point(x / points.size(), y / points.size(), z / points.size());
}

Point center(std::set<Point> &points)
{
    double x = 0;
    double y = 0;
    double z = 0;
    int nb = 0;
    for(std::set<Point>::iterator itr = points.begin(); itr != points.end(); ++itr)
    {
        x += (*itr)[0];
        y += (*itr)[1];
        z += (*itr)[2];
        nb++;
    }
    return Point(x / nb, y / nb, z / nb);
}

void lloyd(Surface *surface, std::vector<Point> &sites, int nb_iters)
{
    for(int i = 0; i < nb_iters; ++i)
    {
        std::cout << "[Lloyd Iter] : " << (i + 1) << " / " << nb_iters << std::endl;
        Clipped_VD cvd(surface, sites);
        cvd.compute();

        if(dynamic_cast<Mesh*>(surface) != NULL)
            for(int j = 0; j < cvd._cells.size(); ++j)
            {
                Voronoi_cell &vc = cvd._cells[j];

                std::vector<Triangle> triangles;
                for(int k = 0; k < vc._bisectors.size(); ++k)
                    triangles.insert(triangles.end(), vc._bisectors[k]._triangles.begin(), vc._bisectors[k]._triangles.end());
                triangles.insert(triangles.end(), vc._rvd.begin(), vc._rvd.end());

                if(triangles.size() == 0)
                    continue;

                Polyhedral_mass_properties pmp(triangles);
                if(std::abs(pmp.T0) > EPSILON)
                    sites[j] = pmp.centroid();
            }

        if(dynamic_cast<Poisson*>(surface) != NULL)
            for(int j = 0; j < cvd._vcells.size(); ++j)
                sites[j] = center(cvd._vcells[j]._points);
    }
}
