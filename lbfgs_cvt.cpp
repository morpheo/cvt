#include "lbfgs_cvt.h"
#include "polyhedral_mass_properties.h"
#include "clipped_vd.h"

int LBFGS_CVT::run()
{
    lbfgsfloatval_t fx;
    lbfgsfloatval_t *m_x = lbfgs_malloc(_sites.size() * 3);

    if (m_x == NULL) {
        printf("ERROR: Failed to allocate a memory block for variables.\n");
        return 1;
    }

    lbfgs_parameter_t param;
    for(int i = 0; i < _sites.size(); ++i)
    {
        m_x[i * 3] = _sites[i][0];
        m_x[i * 3 + 1] = _sites[i][1];
        m_x[i * 3 + 2] = _sites[i][2];
    }

    lbfgs_parameter_init(&param);
    param.max_iterations = NB_ITER;
    param.epsilon = EPSILON;

    int ret = lbfgs(_sites.size() * 3, m_x, &fx, _evaluate, _progress, this, &param);
    return ret;
}

lbfgsfloatval_t LBFGS_CVT::evaluate(const lbfgsfloatval_t *x, lbfgsfloatval_t *g, const int n, const lbfgsfloatval_t step)
{
    for(int i = 0; i < n / 3; ++i)
        if(_surface->is_interior(Point(x[i * 3], x[i * 3 + 1], x[i * 3 + 2])))
            _sites[i] = Point(x[i * 3], x[i * 3 + 1], x[i * 3 + 2]);

    Clipped_VD clipped_vd(_surface, _sites);
    clipped_vd.compute();

    lbfgsfloatval_t fx = 0.0;
    int nb = 0;

    if(dynamic_cast<Mesh*>(_surface) != NULL)
        for(int i = 0; i < clipped_vd._cells.size(); ++i)
        {
            Voronoi_cell &vc = clipped_vd._cells[i];
            std::vector<Triangle> triangles;
            for(int j = 0; j < vc._bisectors.size(); ++j)
                triangles.insert(triangles.end(), vc._bisectors[j]._triangles.begin(), vc._bisectors[j]._triangles.end());
            triangles.insert(triangles.end(), vc._rvd.begin(), vc._rvd.end());

            if(triangles.size() == 0)
                continue;

            Polyhedral_mass_properties pmp(triangles);
            if(std::abs(pmp.T0) < EPSILON)
            {
                g[i*3] = 0;
                g[i*3+1] = 0;
                g[i*3+2] = 0;
            }
            else
            {
                g[i*3] = 2 * pmp.T0 * (x[i*3] - pmp.T1[0]/pmp.T0);
                g[i*3+1] = 2 * pmp.T0 * (x[i*3+1] - pmp.T1[1]/pmp.T0);
                g[i*3+2] = 2 * pmp.T0 * (x[i*3+2] - pmp.T1[2]/pmp.T0);

                fx += pmp.T2[0] - 2*pmp.T1[0]*x[i*3] + x[i*3]*x[i*3]*pmp.T0
                        + pmp.T2[1] - 2*pmp.T1[1]*x[i*3+1] + x[i*3+1]*x[i*3+1]*pmp.T0
                        + pmp.T2[2] - 2*pmp.T1[2]*x[i*3+2] + x[i*3+2]*x[i*3+2]*pmp.T0;
                nb++;
            }
        }

    if(dynamic_cast<Poisson*>(_surface) != NULL)
        for(int i = 0; i < clipped_vd._vcells.size(); ++i)
        {
            if(clipped_vd._vcells[i]._convex_hull.size() != 0)
            {
                std::vector<Triangle> triangles;
                triangles.insert(triangles.end(), clipped_vd._vcells[i]._convex_hull.begin(), clipped_vd._vcells[i]._convex_hull.end());
                triangles.insert(triangles.end(), clipped_vd._vcells[i]._rvd.begin(), clipped_vd._vcells[i]._rvd.end());
                Polyhedral_mass_properties pmp(triangles);
                if(std::abs(pmp.T0) < EPSILON)
                {
                    g[i*3] = 0;
                    g[i*3+1] = 0;
                    g[i*3+2] = 0;
                }
                else
                {
                    g[i*3] = 2 * pmp.T0 * (x[i*3] - pmp.T1[0]/pmp.T0);
                    g[i*3+1] = 2 * pmp.T0 * (x[i*3+1] - pmp.T1[1]/pmp.T0);
                    g[i*3+2] = 2 * pmp.T0 * (x[i*3+2] - pmp.T1[2]/pmp.T0);

                    fx += pmp.T2[0] - 2*pmp.T1[0]*x[i*3] + x[i*3]*x[i*3]*pmp.T0
                            + pmp.T2[1] - 2*pmp.T1[1]*x[i*3+1] + x[i*3+1]*x[i*3+1]*pmp.T0
                            + pmp.T2[2] - 2*pmp.T1[2]*x[i*3+2] + x[i*3+2]*x[i*3+2]*pmp.T0;
                    nb++;
                }
            }
        }

    std::cout << "[Trial] : Fx = " << fx << ", Nb of cells = " << nb << std::endl;
    return fx;
}

int LBFGS_CVT::progress(const lbfgsfloatval_t *x,
                        const lbfgsfloatval_t *g,
                        const lbfgsfloatval_t fx,
                        const lbfgsfloatval_t xnorm,
                        const lbfgsfloatval_t gnorm,
                        const lbfgsfloatval_t step,
                        int n, int k, int ls)
{
    std::cout << "[Iteration] : " << k << std::endl;
    std::cout << "  Fx = " << fx << std::endl;
    std::cout << "  xnorm = " << xnorm << ", gnorm = " << gnorm << ", step = " << step << std::endl;

    int cur = NB_ITER / 10;
    for(int i=1; i<11; ++i)
        if(k == i * cur)
            std::cout << "[Process] : " << i * 10 << "% " << std::endl;
    return 0;
}
