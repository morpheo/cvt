#ifndef POLYHEDRAL_MASS_PROPERTIES_H
#define POLYHEDRAL_MASS_PROPERTIES_H

#include <vector>
#include "types.h"

#define SQR(x) ((x)*(x))
#define CUBE(x) ((x)*(x)*(x))

class Polyhedral_mass_properties
{
public:
    Polyhedral_mass_properties() {}
    Polyhedral_mass_properties(std::vector<Triangle> &ts);

    void compProjectionIntegrals(Triangle t);
    void compFaceIntegrals(Triangle t);
    void compVolumeIntegrals(std::vector<Triangle> &ts);

    Point centroid();

public:
    int A;   /* alpha */
    int B;   /* beta */
    int C;   /* gamma */

    /* projection integrals */
    double P1, Pa, Pb, Paa, Pab, Pbb, Paaa, Paab, Pabb, Pbbb;

    /* face integrals */
    double Fa, Fb, Fc, Faa, Fbb, Fcc, Faaa, Fbbb, Fccc, Faab, Fbbc, Fcca;

    /* volume integrals */
    double T0, T1[3], T2[3], TP[3];
};

#endif // POLYHEDRAL_MASS_PROPERTIES_H
