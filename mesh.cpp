#include "mesh.h"
#include <fstream>
#include <sstream>
#include "helper.h"
#include <ctime>
#include <cstdlib>

Mesh::Mesh(std::string file)
{
    load(file);
    _tree = new Tree(_triangles.begin(), _triangles.end());
}

void Mesh::load(std::string file)
{
    std::ifstream in(file.c_str());
    if(in.is_open())
    {
        std::vector<std::string> tokens;
        split(file, ".", tokens);
        std::string format = tokens[tokens.size() - 1];
        if(format == "off")
        {
            Polyhedron polyhedron;
            in >> polyhedron;
            polyhedron2triangles(polyhedron, _triangles);
            std::cout << "[Surface] : " << file << " (" << polyhedron.size_of_vertices() << ", " << _triangles.size() << ")" << std::endl;
        }
        else if(format == "obj")
        {
            std::string line;
            std::vector<Point> pts;
            std::vector<int> ts;
            while(std::getline(in, line))
            {
                std::istringstream linestream(line);
                std::string label;
                linestream >> label;
                if(label == "v")
                {
                    double x, y, z;
                    linestream >> x;
                    linestream >> y;
                    linestream >> z;
                    pts.push_back(Point(x, y, z));
                }
                if(label == "f")
                {
                    std::string v0, v1, v2;
                    linestream >> v0;
                    linestream >> v1;
                    linestream >> v2;
                    if(v0.find("/") != std::string::npos)
                    {
                        std::vector<std::string> tokens0;
                        split(v0, "/", tokens0);
                        std::vector<std::string> tokens1;
                        split(v1, "/", tokens1);
                        std::vector<std::string> tokens2;
                        split(v2, "/", tokens2);

                        ts.push_back(std::stoi(tokens0[0]) - 1);
                        ts.push_back(std::stoi(tokens1[0]) - 1);
                        ts.push_back(std::stoi(tokens2[0]) - 1);
                    }
                    else
                    {
                        ts.push_back(std::stoi(v0) - 1);
                        ts.push_back(std::stoi(v1) - 1);
                        ts.push_back(std::stoi(v2) - 1);
                    }
                }
            }

            for(int i=0; i<ts.size()/3; ++i)
            {
                Triangle t(pts[ts[i * 3]], pts[ts[i * 3 + 1]], pts[ts[i * 3 + 2]]);
                if(!t.is_degenerate())
                    _triangles.push_back(Triangle(pts[ts[i * 3]], pts[ts[i * 3 + 1]], pts[ts[i * 3 + 2]]));
            }
            std::cout << "[Surface] : " << file << " (" << pts.size() << ", " << _triangles.size() << ")" << std::endl;
        }
        else
            throw std::string("[Error] : cannot load " + file);
    }
    else
        throw std::string("[Error] : cannot load " + file);
}

bool Mesh::is_interior(Point query)
{
    for(unsigned int i=0; i<10; ++i)
    {
        Vector v = random_vector();
        Ray r(query, v);
        int nb_intersections = (int)_tree->number_of_intersected_primitives(r);
        if(nb_intersections % 2 == 0)
            return false;
    }
    return true;
}

void Mesh::interior_sample(int nb, std::vector<Point> &points)
{
    std::srand(std::time(0));
    std::set<Point> pts;
    while(pts.size() < nb)
    {
        Point p = random_point(_tree->bbox());
        if(is_interior(p))
            pts.insert(p);
    }
    points.assign(pts.begin(), pts.end());
}
