#include "clipped_vd.h"
#include "helper.h"
#include <CGAL/convex_hull_3.h>
#include "tetgen.h"

Clipped_VD::Clipped_VD(Surface *surface, std::vector<Point> &sites):
    _surface(surface), _sites(sites)
{
    if(dynamic_cast<Mesh*>(_surface) != NULL)
    {
        _delaunay = new Delaunay_triangulation();
        for(int i=0; i<_sites.size(); ++i)
        {
            Delaunay_triangulation::Vertex_handle vh = _delaunay->insert(_sites[i]);
            vh->info() = i;
            _cells.push_back(Voronoi_cell(i));
        }
    }

    if(dynamic_cast<Poisson*>(_surface) != NULL)
    {
        tetgenio in, out;
        in.numberofpoints = _sites.size();
        in.pointlist = new REAL[in.numberofpoints * 3];
        for(int i = 0; i < _sites.size(); ++i)
        {
            in.pointlist[i * 3] = _sites[i][0];
            in.pointlist[i * 3 + 1] = _sites[i][1];
            in.pointlist[i * 3 + 2] = _sites[i][2];
        }

        tetgenbehavior* opt = new tetgenbehavior();
        opt->quiet = 1;
        opt->voroout = 1;
        tetrahedralize(opt, &in, &out);

        for(int i = 0; i < out.numberofvpoints; ++i)
            _vpoints.push_back(Point(out.vpointlist[i * 3], out.vpointlist[i * 3 + 1], out.vpointlist[i * 3 + 2]));

        for(int i = 0; i < out.numberofvedges; ++i)
        {
            if(out.vedgelist[i].v2 != -1)
                _vedges.push_back(Vedge(out.vedgelist[i].v1, out.vedgelist[i].v2));
            else
                _vedges.push_back(Vedge(out.vedgelist[i].v1, out.vedgelist[i].v2,
                                        Vector(-out.vedgelist[i].vnormal[0], -out.vedgelist[i].vnormal[1], -out.vedgelist[i].vnormal[2])));
        }

        for(int i = 0; i < out.numberofvfacets; ++i)
            _vfaces.push_back(Vface(out.vfacetlist[i].c1, out.vfacetlist[i].c2, out.vfacetlist[i].elist));

        for(int i = 0; i < out.numberofvcells; ++i)
            _vcells.push_back(Vcell(out.vcelllist[i]));

        for(int i = 0; i < _vcells.size(); ++i)
            for(int j = 0; j < _vcells[i]._faces.size(); ++j)
                for(int k = 0; k < _vfaces[_vcells[i]._faces[j]]._edges.size(); ++k)
                    _vedges[_vfaces[_vcells[i]._faces[j]]._edges[k]]._cells.insert(i);
    }
}

void Clipped_VD::compute()
{
    if(dynamic_cast<Mesh*>(_surface) != NULL)
    {
        construct_bisectors();
        clipping();
    }

    if(dynamic_cast<Poisson*>(_surface) != NULL)
    {
        clip_vedges();
        check_vpoints();
        check_vedges();
        check_vfaces();
        check_vcells();
        compute_cells();
    }
}

void Clipped_VD::construct_bisectors()
{
    for(Delaunay_triangulation::Finite_edges_iterator itr = _delaunay->finite_edges_begin();
        itr != _delaunay->finite_edges_end(); ++itr)
    {
        int nb_rays = 0;
        std::vector<Segment> segments;
        Point infinite_point;
        bool is_infinite = false;
        Delaunay_triangulation::Facet_circulator cir = _delaunay->incident_facets(*itr), done(cir);
        do
        {
            if(!_delaunay->is_infinite(*cir))
            {
                CGAL::Object dual = _delaunay->dual(*cir);
                if(CGAL::object_cast<Segment>(&dual))
                    segments.push_back(CGAL::object_cast<Segment>(dual));
                else if(CGAL::object_cast<Ray>(&dual))
                {
                    nb_rays++;
                    Ray r = CGAL::object_cast<Ray>(dual);
                    if(nb_rays == 1)
                        segments.push_back(Segment(r.source(), r.point(1e6)));
                    else if(nb_rays == 2)
                    {
                        infinite_point = r.point(1e6);
                        is_infinite = true;
                    }
                }
                else
                    std::cout << "[Error]: unknown primitive in construct_bisectors()" << std::endl;
            }
        }while(++cir != done);

        if(nb_rays != 0 && nb_rays != 2)
            std::cout << "[Error]: wrong number of rays in construct_bisectors()" << std::endl;

        Bisector bisector(itr->first->vertex(itr->second)->info(), itr->first->vertex(itr->third)->info());
        if(is_infinite)
            for(int j=0; j<segments.size(); ++j)
            {
                Triangle t(infinite_point, segments[j][0], segments[j][1]);
                if(!t.is_degenerate())
                    bisector._triangles.push_back(t);
            }
        else
            for(int j=1; j<segments.size()-1; ++j)
            {
                Triangle t(segments[0][0], segments[j][0], segments[j][1]);
                if(!t.is_degenerate())
                    bisector._triangles.push_back(t);
            }

        _bisectors.push_back(bisector);
    }
}

void Clipped_VD::clipping()
{
    Mesh *surface = dynamic_cast<Mesh*>(_surface);

    std::vector<std::vector<Point_exact> > surface_constraints;
    surface_constraints.resize(surface->_triangles.size());

    // clip_bisectors
#pragma omp parallel for
    for(int i=0; i<_bisectors.size(); ++i)
    {
        int s0 = _bisectors[i]._s0;
        int s1 = _bisectors[i]._s1;
        Bisector b0(s0, s1);
        Bisector b1(s1, s0);

        for(int j=0; j<_bisectors[i]._triangles.size(); ++j)
        {
            Triangle query = _bisectors[i]._triangles[j];

            if(!surface->_tree->do_intersect(query))
                if(surface->is_interior(center(query)))
                {
                    b0._triangles.push_back(orient(_sites[s0], query));
                    b1._triangles.push_back(orient(_sites[s1], query));
                }
                else
                {}
            else
            {
                std::vector<std::vector<Triangle>::iterator> intersected_primitives;
                surface->_tree->all_intersected_primitives(query, std::back_inserter(intersected_primitives));
                std::vector<Point_exact> constraints;
                for(int k=0; k<intersected_primitives.size(); ++k)
                {
                    std::vector<Point_exact> constraints_tmp;
                    intersection(exact(query), exact(*intersected_primitives[k]), constraints_tmp);
                    constraints.insert(constraints.end(), constraints_tmp.begin(), constraints_tmp.end());

#pragma omp critical(insert_surface_constraints)
                    surface_constraints[intersected_primitives[k] - surface->_triangles.begin()].insert(
                                surface_constraints[intersected_primitives[k] - surface->_triangles.begin()].end(),
                            constraints_tmp.begin(), constraints_tmp.end());
                }

                std::vector<Triangle_exact> ts_out;
                cdt_2d(exact(query), constraints, ts_out);
                for(int k=0; k<ts_out.size(); ++k)
                {
                    Triangle t_out = inexact(ts_out[k]);
                    if(!t_out.is_degenerate())
                        if(surface->is_interior(center(t_out)))
                        {
                            b0._triangles.push_back(orient(_sites[s0], t_out));
                            b1._triangles.push_back(orient(_sites[s1], t_out));
                        }
                }
            }
        }
        if(b0._triangles.size() > 0 && b1._triangles.size() > 0)
        {
#pragma omp critical(construct_cells)
            {
                _cells[s0]._neighbors.insert(s1);
                _cells[s1]._neighbors.insert(s0);
                _cells[s0]._bisectors.push_back(b0);
                _cells[s1]._bisectors.push_back(b1);
            }
        }
    }

    // rvd
    std::vector<int> indices;
    for(int i=0; i<_sites.size(); ++i)
        indices.push_back(i);

    Neighbor_search::Tree tree(
                boost::make_zip_iterator(boost::make_tuple(_sites.begin(), indices.begin())),
                boost::make_zip_iterator(boost::make_tuple(_sites.end(), indices.end())));

#pragma omp parallel for
    for(int i=0; i<surface->_triangles.size(); ++i)
    {
        Triangle query = surface->_triangles[i];
        std::vector<Point_exact> &constraints = surface_constraints[i];
        if(constraints.size() == 0)
        {
            Neighbor_search search(tree, center(query));
            int site = boost::get<1>(search.begin()->first);
#pragma omp critical(construct_cells)
            _cells[site]._rvd.push_back(query);
        }
        else
        {
            std::vector<Triangle_exact> ts_out;
            cdt_2d(exact(query), constraints, ts_out);
            for(int j=0; j<ts_out.size(); ++j)
            {
                Triangle t_out = inexact(ts_out[j]);
                if(!t_out.is_degenerate())
                {
                    Neighbor_search search(tree, center(t_out));
                    int site = boost::get<1>(search.begin()->first);
#pragma omp critical(construct_cells)
                    _cells[site]._rvd.push_back(t_out);
                }
            }
        }
    }
}

void Clipped_VD::clip_vedges()
{
    for(int i=0; i<_vedges.size(); ++i)
    {
        Vedge &ve = _vedges[i];
        if(ve._is_infinite)
        {
            ve._ray = ve._ray * (1.0 / std::sqrt(ve._ray.squared_length()));
            Point t = _vpoints[ve._v0] + 1e6 * ve._ray;
            ve._v1 = _vpoints.size();
            _vpoints.push_back(t);
        }
    }
}

void Clipped_VD::check_vpoints()
{
    _values.resize(_vpoints.size());
    for(int i = 0; i < _values.size(); ++i)
        _values[i] = dynamic_cast<Poisson*>(_surface)->value(_vpoints[i]);
}

void Clipped_VD::check_vedges()
{
    for(int i = 0; i < _vedges.size(); ++i)
    {
        Vedge &ve = _vedges[i];
        if(_values[ve._v0] * _values[ve._v1] <= 0)
        {
            ve._do_intersect = true;
            ve._intersection = bisection(dynamic_cast<Poisson*>(_surface), _vpoints[ve._v0], _vpoints[ve._v1]);
        }
    }
}

void Clipped_VD::check_vfaces()
{
    for(int i = 0; i < _vfaces.size(); ++i)
    {
        Vface &vf = _vfaces[i];
        for(int j = 0; j < vf._edges.size(); ++j)
            if(_vedges[vf._edges[j]]._do_intersect)
                vf._intersections.push_back(_vedges[vf._edges[j]]._intersection);
    }
}

void Clipped_VD::check_vcells()
{
    for(int i = 0; i < _vedges.size(); ++i)
    {
        Vedge &ve = _vedges[i];
        for(std::set<int>::iterator itr = ve._cells.begin(); itr != ve._cells.end(); ++itr)
        {
            Vcell &vc = _vcells[*itr];
            if(_values[ve._v0] <= 0)
                vc._points.insert(_vpoints[ve._v0]);
            if(_values[ve._v1] <= 0)
                vc._points.insert(_vpoints[ve._v1]);
            if(ve._do_intersect)
            {
                vc._points.insert(ve._intersection);
                vc._intersections.insert(ve._intersection);
            }
        }
    }
}

void Clipped_VD::compute_cells()
{
#pragma omp parallel for
    for(int i = 0; i < _vcells.size(); ++i)
    {
        Vcell &vc = _vcells[i];
        if(vc._intersections.size() == 0)
        {
            for(int  j = 0; j < vc._faces.size(); ++j)
            {
                if(_vfaces[vc._faces[j]]._edges.size() < 3)
                    continue;

                Vface &vf = _vfaces[vc._faces[j]];
                std::vector<int> vertices;

                int p0 = _vedges[vf._edges[0]]._v0;
                int p1 = _vedges[vf._edges[0]]._v1;
                int p2 = _vedges[vf._edges[1]]._v0;
                int p3 = _vedges[vf._edges[1]]._v1;

                if(p1 == p2 || p1 == p3)
                {
                    vertices.push_back(p0);
                    vertices.push_back(p1);
                }
                else
                {
                    vertices.push_back(p1);
                    vertices.push_back(p0);
                }

                for(int k = 1; k < vf._edges.size() - 1; ++k)
                {
                    if(vertices[vertices.size() - 1] == _vedges[vf._edges[k]]._v0)
                        vertices.push_back(_vedges[vf._edges[k]]._v1);
                    else
                        vertices.push_back(_vedges[vf._edges[k]]._v0);
                }

                Vector v0(_vpoints[vertices[0]], _vpoints[vertices[1]]);
                Vector v1(_vpoints[vertices[0]], _vpoints[vertices[2]]);
                Vector v2(_sites[i], _vpoints[vertices[0]]);
                if(CGAL::cross_product(v0, v1) * v2 < 0)
                    std::reverse(vertices.begin(), vertices.end());

                for(int k = 0; k < vertices.size() - 2; ++k)
                    vc._convex_hull.push_back(Triangle(_vpoints[vertices[0]], _vpoints[vertices[k + 1]], _vpoints[vertices[k + 2]]));
            }
        }
        else
        {
            Polyhedron poly;
            CGAL::convex_hull_3(vc._points.begin(), vc._points.end(), poly);

            std::vector<Triangle> triangles;
            polyhedron2triangles(poly, triangles);

            for(int j = 0; j < triangles.size(); ++j)
            {
                Triangle t = triangles[j];
                if(vc._intersections.find(t[0]) != vc._intersections.end()
                        && vc._intersections.find(t[1]) != vc._intersections.end()
                        && vc._intersections.find(t[2]) != vc._intersections.end())
                    vc._rvd.push_back(t);
                else
                    vc._convex_hull.push_back(t);
            }
        }
    }
}

void Clipped_VD::improvement()
{
#pragma omp parallel for
    for(int i = 0; i < _vcells.size(); ++i)
    {
        Vcell &vc = _vcells[i];
        if(vc._intersections.size() > 0)
        {
            double x = 0;
            double y = 0;
            double z = 0;
            double sum = 0;
            Point r(0, 0, 0);
            for(int j = 0; j < vc._rvd.size(); ++j)
            {
                Triangle t = vc._rvd[j];
                if(!t.is_degenerate())
                {
                    double area = std::sqrt(t.squared_area());
                    sum += area;
                    x += area * (t[0][0] + t[1][0] + t[2][0]) / 3.0;
                    y += area * (t[0][1] + t[1][1] + t[2][1]) / 3.0;
                    z += area * (t[0][2] + t[1][2] + t[2][2]) / 3.0;

                    Vector u(t[0], t[1]);
                    Vector v(t[0], t[2]);
                    Vector n = CGAL::cross_product(u, v);
                    r = r + n;
                }
            }
            Point o(x / sum, y / sum, z / sum);
            Vector ray(r[0], r[1], r[2]);
            ray = ray * (1.0 / std::sqrt(ray.squared_length()));
            bool inside = true;
            double scale = 0.01 * std::sqrt(CGAL::squared_distance(o, _sites[i]));
            if(dynamic_cast<Poisson*>(_surface)->value(o) > 0)
                scale *= -1;
            Point t;
            int count = 0;
            while(inside)
            {
                scale *= 2;
                t = o + scale * ray;
                if(dynamic_cast<Poisson*>(_surface)->value(o) * dynamic_cast<Poisson*>(_surface)->value(t) < 0)
                    inside = false;
                if(count > 100)
                {
                    //std::cout << "[Error] : Improvement" << std::endl;
                    inside = false;
                    break;
                }
                count++;
            }

            if(count > 100)
                continue;

            Point inter = bisection(dynamic_cast<Poisson*>(_surface), o, t);
            if(inter == o)
                continue;

            vc._rvd.clear();
            Point site = center(vc._points);
            for(int j = 0; j < vc._faces.size(); ++j)
            {
                Vface &vf = _vfaces[vc._faces[j]];
                for(int k = 1; k < vf._intersections.size(); ++k)
                {
                    Triangle t_out(inter, vf._intersections[k - 1], vf._intersections[k]);
                    vc._rvd.push_back(orient(site, t_out));
                }
            }
        }
    }
}
