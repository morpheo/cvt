#include "polyhedral_mass_properties.h"
#include <cmath>

Polyhedral_mass_properties::Polyhedral_mass_properties(std::vector<Triangle> &ts)
{
    compVolumeIntegrals(ts);
}

void Polyhedral_mass_properties::compProjectionIntegrals(Triangle t)
{
    double a0, a1, da;
    double b0, b1, db;
    double a0_2, a0_3, a0_4, b0_2, b0_3, b0_4;
    double a1_2, a1_3, b1_2, b1_3;
    double C1, Ca, Caa, Caaa, Cb, Cbb, Cbbb;
    double Cab, Kab, Caab, Kaab, Cabb, Kabb;

    P1 = Pa = Pb = Paa = Pab = Pbb = Paaa = Paab = Pabb = Pbbb = 0.0;

    for(unsigned int i = 0; i < 3; i++)
    {
        a0 = t[i][A];
        b0 = t[i][B];
        a1 = t[(i+1) % 3][A];
        b1 = t[(i+1) % 3][B];
        da = a1 - a0;
        db = b1 - b0;
        a0_2 = a0 * a0; a0_3 = a0_2 * a0; a0_4 = a0_3 * a0;
        b0_2 = b0 * b0; b0_3 = b0_2 * b0; b0_4 = b0_3 * b0;
        a1_2 = a1 * a1; a1_3 = a1_2 * a1;
        b1_2 = b1 * b1; b1_3 = b1_2 * b1;

        C1 = a1 + a0;
        Ca = a1*C1 + a0_2; Caa = a1*Ca + a0_3; Caaa = a1*Caa + a0_4;
        Cb = b1*(b1 + b0) + b0_2; Cbb = b1*Cb + b0_3; Cbbb = b1*Cbb + b0_4;
        Cab = 3*a1_2 + 2*a1*a0 + a0_2; Kab = a1_2 + 2*a1*a0 + 3*a0_2;
        Caab = a0*Cab + 4*a1_3; Kaab = a1*Kab + 4*a0_3;
        Cabb = 4*b1_3 + 3*b1_2*b0 + 2*b1*b0_2 + b0_3;
        Kabb = b1_3 + 2*b1_2*b0 + 3*b1*b0_2 + 4*b0_3;

        P1 += db*C1;
        Pa += db*Ca;
        Paa += db*Caa;
        Paaa += db*Caaa;
        Pb += da*Cb;
        Pbb += da*Cbb;
        Pbbb += da*Cbbb;
        Pab += db*(b1*Cab + b0*Kab);
        Paab += db*(b1*Caab + b0*Kaab);
        Pabb += da*(a1*Cabb + a0*Kabb);
    }

    P1 /= 2.0;
    Pa /= 6.0;
    Paa /= 12.0;
    Paaa /= 20.0;
    Pb /= -6.0;
    Pbb /= -12.0;
    Pbbb /= -20.0;
    Pab /= 24.0;
    Paab /= 60.0;
    Pabb /= -60.0;
}

void Polyhedral_mass_properties::compFaceIntegrals(Triangle t)
{
    double w;
    double k1, k2, k3, k4;

    compProjectionIntegrals(t);

    Vector v0(t[0], t[1]);
    Vector v1(t[0], t[2]);
    Vector n = CGAL::cross_product(v0, v1);
    double len = std::sqrt(n[0]*n[0] + n[1]*n[1] + n[2]*n[2]);
    if(std::isnan(len))
        std::cout << "len = " << len << std::endl;
    n = n / len;

    w = - n[0] * t[0][0] - n[1] * t[0][1] - n[2] * t[0][2];
    k1 = 1 / n[C]; k2 = k1 * k1; k3 = k2 * k1; k4 = k3 * k1;

    Fa = k1 * Pa;
    Fb = k1 * Pb;
    Fc = -k2 * (n[A]*Pa + n[B]*Pb + w*P1);

    Faa = k1 * Paa;
    Fbb = k1 * Pbb;
    Fcc = k3 * (SQR(n[A])*Paa + 2*n[A]*n[B]*Pab + SQR(n[B])*Pbb
                + w*(2*(n[A]*Pa + n[B]*Pb) + w*P1));

    Faaa = k1 * Paaa;
    Fbbb = k1 * Pbbb;
    Fccc = -k4 * (CUBE(n[A])*Paaa + 3*SQR(n[A])*n[B]*Paab
                  + 3*n[A]*SQR(n[B])*Pabb + CUBE(n[B])*Pbbb
                  + 3*w*(SQR(n[A])*Paa + 2*n[A]*n[B]*Pab + SQR(n[B])*Pbb)
                  + w*w*(3*(n[A]*Pa + n[B]*Pb) + w*P1));

    Faab = k1 * Paab;
    Fbbc = -k2 * (n[A]*Pabb + n[B]*Pbbb + w*Pbb);
    Fcca = k3 * (SQR(n[A])*Paaa + 2*n[A]*n[B]*Paab + SQR(n[B])*Pabb
                 + w*(2*(n[A]*Paa + n[B]*Pab) + w*Pa));
}

void Polyhedral_mass_properties::compVolumeIntegrals(std::vector<Triangle> &ts)
{
    double nx, ny, nz;

    T0 = T1[0] = T1[1] = T1[2]
            = T2[0] = T2[1] = T2[2]
            = TP[0] = TP[1] = TP[2] = 0;

    for(unsigned int i = 0; i < ts.size(); i++)
    {
        Vector v0(ts[i][0], ts[i][1]);
        Vector v1(ts[i][0], ts[i][2]);
        Vector n = CGAL::cross_product(v0, v1);
        double len = std::sqrt(n[0]*n[0] + n[1]*n[1] + n[2]*n[2]);

        if(len == 0)
            continue;

        n = n / len;

        nx = std::abs(n[0]);
        ny = std::abs(n[1]);
        nz = std::abs(n[2]);
        if (nx > ny && nx > nz) C = 0;
        else C = (ny > nz) ? 1 : 2;
        A = (C + 1) % 3;
        B = (A + 1) % 3;

        compFaceIntegrals(ts[i]);

        T0 += n[0] * ((A == 0) ? Fa : ((B == 0) ? Fb : Fc));

        T1[A] += n[A] * Faa;
        T1[B] += n[B] * Fbb;
        T1[C] += n[C] * Fcc;
        T2[A] += n[A] * Faaa;
        T2[B] += n[B] * Fbbb;
        T2[C] += n[C] * Fccc;
        TP[A] += n[A] * Faab;
        TP[B] += n[B] * Fbbc;
        TP[C] += n[C] * Fcca;
    }

    T1[0] /= 2; T1[1] /= 2; T1[2] /= 2;
    T2[0] /= 3; T2[1] /= 3; T2[2] /= 3;
    TP[0] /= 2; TP[1] /= 2; TP[2] /= 2;
}

Point Polyhedral_mass_properties::centroid()
{
    return Point(T1[0]/T0, T1[1]/T0, T1[2]/T0);
}
