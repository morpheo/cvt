#ifndef CLIPPED_VD_H
#define CLIPPED_VD_H

#include "types.h"
#include <vector>
#include "surface.h"
#include "poisson.h"
#include "mesh.h"

class Bisector
{
public:
    Bisector(int s0, int s1): _s0(s0), _s1(s1) {}

public:
    int _s0;
    int _s1;
    std::vector<Triangle> _triangles;
};

class Voronoi_cell
{
public:
    Voronoi_cell(int id): _id(id) {}

public:
    int _id;
    std::set<int> _neighbors;
    std::vector<Bisector> _bisectors;
    std::vector<Triangle> _rvd;
};

class Vedge
{
public:
    Vedge(int v0, int v1, Vector ray = Vector(0, 0, 0)): _v0(v0), _v1(v1), _ray(ray), _is_infinite(false), _do_intersect(false)
    {
        if(_v1 == -1)
            _is_infinite = true;
    }

public:
    int _v0;
    int _v1;
    Vector _ray;
    bool _is_infinite;
    std::set<int> _cells;
    bool _do_intersect;
    Point _intersection;
};

class Vface
{
public:
    Vface(int c0, int c1, int *elist): _c0(c0), _c1(c1), _is_infinite(false)
    {
        int nb = elist[0];
        for(int i=1; i<nb; ++i)
            _edges.push_back(elist[i]);
        if(elist[nb] == -1)
            _is_infinite = true;
        else
            _edges.push_back(elist[nb]);
    }

public:
    int _c0;
    int _c1;
    std::vector<int> _edges;
    bool _is_infinite;
    std::vector<Point> _intersections;
};

class Vcell
{
public:
    Vcell(int *clist): _is_infinite(false)
    {
        int nb = clist[0];
        for(int i=1; i<nb; ++i)
            _faces.push_back(clist[i]);
        if(clist[nb] == -1)
            _is_infinite = true;
        else
            _faces.push_back(clist[nb]);
    }

public:
    std::vector<int> _faces;
    bool _is_infinite;
    std::set<Point> _intersections;
    std::set<Point> _points;
    std::vector<Triangle> _convex_hull;
    std::vector<Triangle> _rvd;
};

class Clipped_VD
{
public:
    Clipped_VD(Surface *surface, std::vector<Point> &sites);

    void compute();

    void construct_bisectors();
    void clipping();

    void clip_vedges();
    void check_vpoints();
    void check_vedges();
    void check_vfaces();
    void check_vcells();
    void compute_cells();
    void improvement();

public:
    Surface *_surface;
    Delaunay_triangulation *_delaunay;
    std::vector<Point> _sites;
    std::vector<Bisector> _bisectors;
    std::vector<Voronoi_cell> _cells;

    std::vector<Point> _vpoints;
    std::vector<double> _values;
    std::vector<Vedge> _vedges;
    std::vector<Vface> _vfaces;
    std::vector<Vcell> _vcells;
};

#endif // CLIPPED_VD_H
