#include <iostream>
#include <string>
#include "mesh.h"
#include "poisson.h"
#include "helper.h"
#include "clipped_vd.h"
#include "lbfgs_cvt.h"

int main(int argc, char **argv)
{   
    if(argc < 2)
    {
        std::cerr << "[Error] : no input file" << std::endl;
        return 1;
    }

    std::string input = argv[1];
    if(input == "-h")
    {
        std::cout << "Usage: ./CVT file_name [nb_sites] [nb_iterations] [shrink_factor]" << std::endl;
        std::cout << "Ex: ./CVT ../data/sphere.xyz 3000 30 0.8" << std::endl;
        return 1;
    }

    std::vector<std::string> tokens;
    split(input, ".", tokens);
    std::string format;
    if(tokens.size() > 0)
        format = tokens[tokens.size() - 1];
    else
    {
        std::cerr << "[Error] : unknown input file " << input << std::endl;
        return 1;
    }

    if(argc > 2)
    {
        NB_SITES = std::atoi(argv[2]);
        if(NB_SITES < 1)
        {
            std::cerr << "[Error] : nb_sites need to be strictly positive" << std::endl;
            return 1;
        }
    }
    if(argc > 3)
    {
        NB_ITER = std::atoi(argv[3]);
        if(NB_ITER < 1)
        {
            std::cerr << "[Error] : nb_iterations need to be strictly positive" << std::endl;
            return 1;
        }
    }
    if(argc > 4)
    {
        SHRINK = std::atof(argv[4]);
        if(SHRINK <= 0 || SHRINK > 1)
        {
            std::cerr << "[Error] : shrink_factor need to be in (0, 1]" << std::endl;
            return 1;
        }
    }
    if(argc > 5)
    {
        std::cerr << "[Error] : too many arguments" << std::endl;
        return 1;
    }

    Surface *surface = NULL;
    if(format == "off" || format == "obj")
    {
        try{
            surface = new Mesh(input);
        }catch(std::string e){
            std::cerr << e << std::endl;
            return 1;
        }
    }
    else if(format == "xyz")
    {
        try{
            surface = new Poisson(input);
        }catch(std::string e){
            std::cerr << e << std::endl;
            return 1;
        }
    }
    else
    {
        std::cerr << "[Error] : unknown input file " << input << std::endl;
        return 1;
    }

    std::vector<Point> sites;
    surface->interior_sample(NB_SITES, sites);

    lloyd(surface, sites);

    LBFGS_CVT lbfgs_cvt(surface, sites);
    lbfgs_cvt.run();
    save("centroids.xyz", sites);

    Clipped_VD cvd(surface, sites);
    cvd.compute();

    if(dynamic_cast<Poisson*>(surface) != NULL)
        cvd.improvement();
    save("cvd.obj", cvd, SHRINK);

    return 0;
}

