#ifndef POISSON_H
#define POISSON_H

#include "surface.h"
#include "types.h"
#include <vector>

class Poisson: public Surface
{
public:
    Poisson(std::string file);

    bool is_interior(Point query);
    void interior_sample(int nb, std::vector<Point> &points);
    double value(Point query);

public:
    std::vector<Point_with_normal> _points;
    Poisson_reconstruction_function *_implicit_function;
    Bbox _bbox;
};

#endif // POISSON_H
